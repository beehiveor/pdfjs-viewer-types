import * as _pdfjsLib from "pdfjs-viewer-build/types/src/pdf";
import type { AppOptions } from "pdfjs-viewer-build/types/web/app_options";
import type { App } from "./types/app";

declare global {
  var PDFViewerApplication: App | undefined;
  var PDFViewerApplicationOptions: AppOptions | undefined;
  var pdfjsLib: typeof _pdfjsLib | undefined;
}

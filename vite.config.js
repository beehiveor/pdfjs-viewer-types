import { defineConfig } from "vite";
import pdfViewer from "vite-plugin-pdfjs-viewer";

export default defineConfig({
  plugins: [ pdfViewer() ]
})
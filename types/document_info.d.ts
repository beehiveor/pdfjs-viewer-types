export interface DocumentInfo {
  CreationDate: string;
  Creator: string;
  EncryptFilterName: string | null;
  IsAcroFormPresent: boolean;
  IsCollectionPresent: boolean;
  IsLinearized: boolean;
  IsSignaturesPresent: boolean;
  IsXFAPresent: boolean;
  Language: string | null;
  ModDate: string;
  PDFFormatVersion: string;
  Producer: string;
  Title: string;
}
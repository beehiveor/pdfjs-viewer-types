import { PDFFindController, PDFPageView, PDFViewer } from "pdfjs-viewer-build";

export namespace PDFEvent {
  export interface Base<S> { source: S }
  export interface ListenerOptions { once: boolean }
  export type Listener<D> = (data: D) => any;

  export interface RotationChanging extends Base<PDFViewer> {
    pagesRotation: number;
    pageNumber: number;
  }

  export interface ScaleChanging extends Base<PDFViewer> {
    scale: number;
    presetValue?: any;
  }

  export interface PageChanging extends Base<PDFViewer> {
    pageNumber: number;
    pageLabel: string | null;
    previous: number;
  }

  export interface HashChange extends Base<Window> {
    hash: string,
  }

  export interface UpdateViewArea extends Base<PDFViewer> {
    location: {
      left: number;
      pageNumber: number;
      pdfOpenParams: string;
      rotation: number;
      scale: number;
      top: number;
    }
  }

  export interface PageRendered extends Base<PDFPageView> {
    timestamp: number;
    pageNumber: number;
    cssTransform: boolean;
    error: any;
  }

  export interface PagesLoaded extends Base<PDFViewer> {
    pagesCount: number;
  }

  export interface Find extends Base<unknown> {
    caseSensitive: boolean;
    entireWord: boolean;
    findPrevious: boolean;
    highlightAll: boolean;
    matchDiacritics: boolean;
    phraseSearch: boolean;
    query: string;
    type: "highlightallchange"
    | "casesensitivitychange"
    | "diacriticmatchingchange"
    | "entirewordchange"
    | "again";
  }

  export type FindBarClose = Base<unknown>;

  export interface UpdateFindMatchesCount extends Base<PDFFindController> {
    matchesCount: {
      current: number;
      total: number;
    }
  }

  export interface UpdateFindControlState extends UpdateFindMatchesCount {
    previous: boolean;
    rawQuery: string;
    state: 0 | 1 | 2 | 3;
  }

  export interface ToolbarAction extends Base<unknown> {
    value?: string;
  }

  export interface SecondaryToolbarAction extends Base<unknown> {}

  export type PageNumberChanged = Required<ToolbarAction>;
  export type ScaleChanged = Required<ToolbarAction>;

  export interface ScrollModeChanged extends Base<PDFViewer> {
    mode: 0 | 1 | 2 | 3
  }

  export interface SpreadModeChanged extends Base<PDFViewer> {
    mode: 0 | 1 | 2
  }

  export interface SwitchScrollMode extends SecondaryToolbarAction, Omit<ScrollModeChanged, "source"> {}
  export interface SwitchSpreadMode extends SecondaryToolbarAction, Omit<SpreadModeChanged, "source"> {}

  export interface FileInputChange extends Base<HTMLInputElement> {
    fileInput: HTMLInputElement,
  }

  export type Resize = Base<globalThis.Window | HTMLElement>;

  export interface SidebarViewChanged extends Base<unknown> {
    view: 0 | 1 | 2 | 3
  }

  export interface PresentationModeChanged extends Base<unknown> {
    state: 0 | 1 | 2 | 3
  }

  export interface CursorToolChanged extends SecondaryToolbarAction {
    tool: 0 | 1
  }
}

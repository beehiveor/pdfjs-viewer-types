import { EventBus as _EventBus } from "pdfjs-viewer-build";
import { EventMap } from "./event_map";
import { PDFEvent } from "./event_utils";

export interface EventBus<M = EventMap> extends Omit<_EventBus, "dispatch" | "on" | "off"> {
  dispatch<N extends keyof M, D = M[N]>(name: N, data: D): void;
  on<N extends keyof M, D = M[N]>(name: N, listener: PDFEvent.Listener<D>, options?: PDFEvent.ListenerOptions): void;
  off<N extends keyof M, D = M[N]>(name: N, listener: PDFEvent.Listener<D>, options?: PDFEvent.ListenerOptions): void;
}
import { PDFEvent } from "./event_utils";

export interface EventMap {
  fileinputchange: PDFEvent.FileInputChange,
  hashchange: PDFEvent.HashChange,
  optionalcontentconfig: PDFEvent.Base<unknown>,
  pagemode: PDFEvent.Base<unknown>,
  namedaction: PDFEvent.Base<unknown>,
  save: PDFEvent.Base<unknown>,
  zoomreset: PDFEvent.Base<unknown>,
  findfromurlhash: PDFEvent.Base<unknown>,
  findbarclosed: PDFEvent.Base<unknown>,

  // # Print
  beforeprint: PDFEvent.Base<Window>,
  afterprint: PDFEvent.Base<Window>,

  // # Page
  pagesloaded: PDFEvent.PagesLoaded,
  pagerendered: PDFEvent.PageRendered,
  pagechanging: PDFEvent.PageChanging,
  pagenumberchanged: PDFEvent.PageNumberChanged,

  // # View
  resize: PDFEvent.Resize,
  updateviewarea: PDFEvent.UpdateViewArea,
  scalechanging: PDFEvent.ScaleChanging,
  rotationchanging: PDFEvent.RotationChanging,
  sidebarviewchanged: PDFEvent.SidebarViewChanged,
  presentationmodechanged: PDFEvent.PresentationModeChanged,
  scrollmodechanged: PDFEvent.ScrollModeChanged,
  spreadmodechanged: PDFEvent.SpreadModeChanged,

  // # Find
  find: PDFEvent.Find,
  updatefindmatchescount: PDFEvent.UpdateFindMatchesCount,
  updatefindcontrolstate: PDFEvent.UpdateFindControlState,
  findbarclose: PDFEvent.FindBarClose,

  // # Toolbar
  nextpage: PDFEvent.ToolbarAction,
  previouspage: PDFEvent.ToolbarAction,
  zoomout: PDFEvent.ToolbarAction,
  zoomin: PDFEvent.ToolbarAction,
  scalechanged: PDFEvent.ScaleChanged,
  presentationmode: PDFEvent.ToolbarAction,
  openfile: PDFEvent.ToolbarAction,
  print: PDFEvent.ToolbarAction
  download: PDFEvent.ToolbarAction,

  // # Secondary Toolbar
  firstpage: PDFEvent.SecondaryToolbarAction,
  lastpage: PDFEvent.SecondaryToolbarAction,
  rotatecw: PDFEvent.SecondaryToolbarAction,
  rotateccw: PDFEvent.SecondaryToolbarAction,
  cursortoolchanged: PDFEvent.CursorToolChanged,
  switchscrollmode: PDFEvent.SwitchScrollMode,
  switchspreadmode: PDFEvent.SwitchSpreadMode,
  documentproperties: PDFEvent.SecondaryToolbarAction,
}

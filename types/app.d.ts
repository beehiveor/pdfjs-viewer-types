import { DownloadManager, PDFHistory, PDFLinkService, PDFScriptingManager, PDFViewer } from "pdfjs-viewer-build";
import { PDFDocumentProxy, PDFDocumentLoadingTask } from "pdfjs-viewer-build/types/src/pdf";
import { PDFThumbnailViewer } from "pdfjs-viewer-build/types/web/pdf_thumbnail_viewer";
import { PDFRenderingQueue } from "pdfjs-viewer-build/types/web/pdf_rendering_queue";
import { Metadata } from "pdfjs-viewer-build/types/src/display/metadata";
import { IL10n } from "pdfjs-viewer-build/types/web/interfaces";

import { EventBus } from "./event_bus";
import { AppConfig } from "./app_config";
import { DocumentInfo } from "./document_info";
import { Preferences } from "./preferences";

export interface App {
  initialBookmark: string;
  appConfig: AppConfig | null;
  pdfDocument: PDFDocumentProxy;
  pdfLoadingTask: PDFDocumentLoadingTask;
  printService: unknown;
  pdfViewer: PDFViewer | null,
  pdfThumbnailViewer: PDFThumbnailViewer | null;
  pdfRenderingQueue: PDFRenderingQueue | null;
  pdfPresentationMode: unknown;
  pdfDocumentProperties: unknown;
  pdfLinkService: PDFLinkService;
  pdfHistory: PDFHistory;
  pdfSidebar: unknown;
  pdfSidebarResizer: unknown;
  pdfOutlineViewer: unknown;
  pdfAttachmentViewer: unknown;
  pdfLayerViewer: unknown;
  pdfCursorTools: unknown;
  pdfScriptingManager: PDFScriptingManager;
  store: unknown;
  downloadManager: DownloadManager;
  overlayManager: unknown;
  preferences: Preferences | null,
  toolbar: unknown;
  secondaryToolbar: unknown;
  eventBus: EventBus | null,
  l10n: IL10n;
  isInitialViewSet: boolean;
  downloadComplete: boolean;
  isViewerEmbedded: boolean;
  url: string;
  baseUrl: string;
  externalServices: unknown;
  documentInfo: DocumentInfo | null;
  metadata: Metadata;

  findBar?: unknown;

  open(file: string | ArrayBufferView, args?: Record<string, any>): Promise<void>;

  get initializedPromise(): Promise<void>;
}
export interface AppConfig {
  appContainer: HTMLBodyElement,
  debuggerScriptPath: string,
  documentProperties: {
    closeButton: HTMLButtonElement,
    container: HTMLDivElement,
    fields: {
      author: HTMLParagraphElement,
      creationDate: HTMLParagraphElement,
      creator: HTMLParagraphElement,
      fileName: HTMLParagraphElement,
      fileSize: HTMLParagraphElement,
      keywords: HTMLParagraphElement,
      linearized: HTMLParagraphElement,
      modificationDate: HTMLParagraphElement,
      pageCount: HTMLParagraphElement,
      pageSize: HTMLParagraphElement,
      producer: HTMLParagraphElement,
      subject: HTMLParagraphElement,
      title: HTMLParagraphElement,
      version: HTMLParagraphElement,
    },
    overlayName: string,
  },
  errorWrapper: {
    closeButton: HTMLButtonElement,
    container: HTMLDivElement,
    errorMessage: HTMLSpanElement,
    errorMoreInfo: HTMLTextAreaElement,
    lessInfoButton: HTMLButtonElement,
    moreInfoButton: HTMLButtonElement,
  },
  findBar: {
    bar: HTMLDivElement,
    caseSensitiveCheckbox: HTMLInputElement,
    entireWordCheckbox: HTMLInputElement,
    findField: HTMLInputElement,
    findMsg: HTMLSpanElement,
    findNextButton: HTMLButtonElement,
    findPreviousButton: HTMLButtonElement,
    findResultsCount: HTMLSpanElement,
    highlightAllCheckbox: HTMLInputElement,
    matchDiacriticsCheckbox: HTMLInputElement,
    toggleButton: HTMLButtonElement,
  },
  mainContainer: HTMLDivElement,
  openFileInputName: string,
  passwordOverlay: {
    cancelButton: HTMLButtonElement,
    container: HTMLDivElement,
    input: HTMLInputElement,
    label: HTMLParagraphElement,
    overlayName: string,
    submitButton: HTMLButtonElement,
  },
  printContainer: HTMLDivElement,
  secondaryToolbar: {
    cursorHandToolButton: HTMLButtonElement,
    cursorSelectToolButton: HTMLButtonElement,
    documentPropertiesButton: HTMLButtonElement,
    downloadButton: HTMLButtonElement,
    firstPageButton: HTMLButtonElement,
    lastPageButton: HTMLButtonElement,
    openFileButton: HTMLButtonElement,
    pageRotateCcwButton: HTMLButtonElement,
    pageRotateCwButton: HTMLButtonElement,
    presentationModeButton: HTMLButtonElement,
    printButton: HTMLButtonElement,
    scrollHorizontalButton: HTMLButtonElement,
    scrollPageButton: HTMLButtonElement,
    scrollVerticalButton: HTMLButtonElement,
    scrollWrappedButton: HTMLButtonElement,
    spreadEvenButton: HTMLButtonElement,
    spreadNoneButton: HTMLButtonElement,
    spreadOddButton: HTMLButtonElement,
    toggleButton: HTMLButtonElement,
    toolbar: HTMLDivElement,
    toolbarButtonContainer: HTMLDivElement,
    viewBookmarkButton: HTMLAnchorElement,
  },
  sidebar: {
    attachmentsButton: HTMLButtonElement,
    attachmentsView: HTMLDivElement,
    currentOutlineItemButton: HTMLButtonElement,
    layersButton: HTMLButtonElement,
    layersView: HTMLDivElement,
    outerContainer: HTMLDivElement,
    outlineButton: HTMLButtonElement,
    outlineOptionsContainer: HTMLDivElement,
    outlineView: HTMLDivElement,
    thumbnailButton: HTMLButtonElement,
    thumbnailView: HTMLDivElement,
    toggleButton: HTMLButtonElement,
    viewerContainer: HTMLDivElement,
  },
  sidebarResizer: {
    outerContainer: HTMLDivElement,
    resizer: HTMLDivElement
  },
  toolbar: {
    container: HTMLDivElement,
    customScaleOption: HTMLOptionElement,
    download: HTMLButtonElement,
    next: HTMLButtonElement,
    numPages: HTMLSpanElement,
    openFile: HTMLButtonElement,
    pageNumber: HTMLInputElement,
    presentationModeButton: HTMLButtonElement,
    previous: HTMLButtonElement,
    print: HTMLButtonElement,
    scaleSelect: HTMLSelectElement,
    viewBookmark: HTMLAnchorElement,
    viewFind: HTMLButtonElement,
    zoomIn: HTMLButtonElement,
    zoomOut: HTMLButtonElement,
  },
  viewerContainer: HTMLDivElement,
}

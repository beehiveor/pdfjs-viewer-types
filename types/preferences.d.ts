
export interface Preferences {
  defaults: Record<string, string | number | boolean>,
  prefs: Record<string, string | number | boolean>,
}
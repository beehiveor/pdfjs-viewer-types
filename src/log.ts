import { PDFPreferences, PDFViewerApplication, EventBus } from "../types";

export function logAppInfo(app: PDFViewerApplication, log = console.info) {
  console.group("app");
  log("url:", app.url);
  log("baseUrl:", app.baseUrl);
  log("initialBookmark:", app.initialBookmark);
  log("isInitialViewSet:", app.isInitialViewSet);
  log("isViewerEmbedded:", app.isViewerEmbedded);

  if (app.preferences) {
    logPreferences(app.preferences, log);
  }

  console.groupEnd();
}

export function logPreferences(preferences: PDFPreferences, log = console.info) {
  console.groupCollapsed("preferences");

  console.group("defaults");
  logRecord(preferences.defaults, log);

  console.group("prefs");
  logRecord(preferences.prefs, log);

  console.groupEnd();
  console.groupEnd();
  console.groupEnd();
}

export function logRecord(o: Record<string, any>, log = console.info) {
  for (let [key, value] of Object.entries(o)) {
    log(key + ":", value);
  }
}

export function bindLogEvents(b: EventBus, log = console.info) {
  b.on("fileinputchange", e => log("fileinputchange", e));
  b.on("hashchange", e => log("hashchange", e));
  b.on("optionalcontentconfig", e => log("optionalcontentconfig", e));
  b.on("pagemode", e => log("pagemode", e));
  b.on("namedaction", e => log("namedaction", e));
  b.on("save", e => log("save", e));
  b.on("zoomreset", e => log("zoomreset", e));
  b.on("findfromurlhash", e => log("findfromurlhash", e));
  b.on("findbarclosed", e => log("findbarclosed", e));

  // # Print
  b.on("beforeprint", e => log("beforeprint", e));
  b.on("afterprint", e => log("afterprint", e));
  
  // # Page
  b.on("pagesloaded", e => log("pagesloaded", e));
  b.on("pagerendered", e => log("pagerendered", e));
  b.on("pagechanging", e => log("pagechanging", e));
  b.on("pagenumberchanged", e => log("pagenumberchanged", e));

  // # View
  b.on("resize", e => log("resize", e));
  b.on("updateviewarea", e => log("updateviewarea", e));
  b.on("scalechanging", e => log("scalechanging", e));
  b.on("rotationchanging", e => log("rotationchanging", e));
  b.on("sidebarviewchanged", e => log("sidebarviewchanged", e));
  b.on("presentationmodechanged", e => log("presentationmodechanged", e));
  b.on("scrollmodechanged", e => log("scrollmodechanged", e));
  b.on("spreadmodechanged", e => log("spreadmodechanged", e));

  // # Find
  b.on("find", e => log("find", e));
  b.on("updatefindmatchescount", e => log("updatefindmatchescount", e));
  b.on("updatefindcontrolstate", e => log("updatefindcontrolstate", e));
  b.on("findbarclose", e => log("findbarclose", e));

  // # Toolbar
  b.on("nextpage", e => log("nextpage", e));
  b.on("previouspage", e => log("previouspage", e));
  b.on("zoomout", e => log("zoomout", e));
  b.on("zoomin", e => log("zoomin", e));
  b.on("scalechanged", e => log("scalechanged", e));
  b.on("presentationmode", e => log("presentationmode", e));
  b.on("openfile", e => log("openfile", e));
  b.on("print", e => log("print", e));
  b.on("download", e => log("download", e));

  // # Secondary Toolbar
  b.on("firstpage", e => log("firstpage", e));
  b.on("lastpage", e => log("lastpage", e));
  b.on("rotatecw", e => log("rotatecw", e));
  b.on("rotateccw", e => log("rotateccw", e));
  b.on("cursortoolchanged", e => log("cursortoolchanged", e));
  b.on("switchscrollmode", e => log("switchscrollmode", e));
  b.on("switchspreadmode", e => log("switchspreadmode", e));
  b.on("documentproperties", e => log("documentproperties", e));
}
import { bindLogEvents, logAppInfo } from "./log";

if (!PDFViewerApplication) throw new Error("PDFViewerApplication is not loaded");
const app = PDFViewerApplication!;

window.addEventListener("load", async () => {
  bindLogEvents(app.eventBus!);

  await app.initializedPromise;

  logAppInfo(PDFViewerApplication);

  await app.open("sample.pdf");
})